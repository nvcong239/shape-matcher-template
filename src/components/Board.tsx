import React, { useState, useEffect } from "react";
import Cell from "./Cell";
import "./Board.css";
import { CellColor, CellShape, CellType } from "../types";

function generateRandomObjects() {
  const colors: CellColor[] = ["red", "green", "blue"];
  const shapes: CellShape[] = ["circle", "square", "triangle"];
  const randomObjects = [];
  for (let i = 0; i < 8; i++) {
    const randomColor = colors[Math.floor(Math.random() * colors.length)];
    const randomShape = shapes[Math.floor(Math.random() * shapes.length)];
    const obj1 = {
      id: i + 1,
      color: randomColor,
      shape: randomShape,
      isOpen: false,
      isDone: false,
    };
    const obj2 = {
      id: i + 1 + 8,
      color: randomColor,
      shape: randomShape,
      isOpen: false,
      isDone: false,
    };
    randomObjects.push(obj1);
    randomObjects.push(obj2);
  }
  for (let i = randomObjects.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [randomObjects[i], randomObjects[j]] = [randomObjects[j], randomObjects[i]];
  }
  return randomObjects;
}

const listCellInit = generateRandomObjects();

const Board: React.FC = () => {
  const [listCell, setListCell] = useState<CellType[]>(listCellInit);
  const [isChecking, setIsChecking] = useState(false);
  const [isGameOver, setIsGameOver] = useState(false);

  const handleCellClick = (cell: CellType) => {
    if (cell.isDone || cell.isOpen || isChecking) return;

    setIsChecking(true);
    setListCell((prev) => {
      const oldCell = prev.find((item) => item.id === cell.id);
      return prev.map((item) => {
        if (item.id === oldCell?.id)
          return {
            ...oldCell,
            isOpen: true,
            isDone: false,
          };
        return item;
      });
    });
  };

  useEffect(() => {
    if (isGameOver) return;
    if (listCell.every((item) => item.isDone)) {
      setIsGameOver(true);
    }

    // check for matches
    const cells = listCell.filter((item) => item.isOpen && !item.isDone);
    if (cells.length === 2) {
      if (
        cells[0].color === cells[1].color &&
        cells[0].shape === cells[1].shape
      ) {
        setIsChecking(false);
        setListCell((prev) =>
          prev.map((item) => {
            if (item.id === cells[0].id)
              return {
                ...cells[0],
                isOpen: true,
                isDone: true,
              };
            if (item.id === cells[1]?.id)
              return {
                ...cells[1],
                isOpen: true,
                isDone: true,
              };
            return item;
          })
        );
      } else {
        setTimeout(() => {
          setIsChecking(false);
          setListCell((prev) =>
            prev.map((item) => {
              if (item.id === cells[0].id)
                return {
                  ...cells[0],
                  isOpen: false,
                  isDone: false,
                };
              if (item.id === cells[1]?.id)
                return {
                  ...cells[1],
                  isOpen: false,
                  isDone: false,
                };
              return item;
            })
          );
        }, 1000);
      }
    } else {
      setIsChecking(false);
    }
  }, [listCell, isGameOver]);

  return (
    <>
      {isGameOver && <p>Game over!</p>}
      <div className="board">
        {listCell.map((cell) => (
          <Cell cell={cell} onClick={handleCellClick} />
        ))}
      </div>
    </>
  );
};

export default Board;
