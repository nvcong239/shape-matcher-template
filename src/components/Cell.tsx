import React from "react";
import "./Cell.css";
import { CellType } from "../types";
import clsx from "clsx";

interface CellProps {
  cell: CellType;
  onClick: (cell: CellType) => void;
}

const Cell: React.FC<CellProps> = ({ cell, onClick }: CellProps) => {
  // Render cell with shape and color, use CSS to style based on shape and color.
  return (
    <div
      onClick={() => onClick(cell)}
      className={clsx({
        red: cell.color === "red" && cell.isOpen,
        blue: cell.color === "blue" && cell.isOpen,
        green: cell.color === "green" && cell.isOpen,

        notOpen: !cell.isOpen,

        circle: cell.shape === "circle" && cell.isOpen,
        square: cell.shape === "square" && cell.isOpen,
        triangle: cell.shape === "triangle" && cell.isOpen,

        cell,
      })}
    />
  );
};

export default Cell;
