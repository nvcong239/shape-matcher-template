// your types here

export type CellColor = "red" | "green" | "blue";
export type CellShape = "circle" | "square" | "triangle";

export type CellType = {
  id: number;
  color: CellColor;
  shape: CellShape;
  isOpen: boolean;
  isDone: boolean;
};
